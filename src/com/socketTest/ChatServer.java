package com.socketTest;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServer implements Runnable {

    private ServerSocket server;
    private Thread thread;
    private Session client;

    public ChatServer(int port) {
	try {
	    System.out.println("Waiting for client...\n");
	    // Start the server
	    server = new ServerSocket(port);
	    // Begin a session with a client
	    start();

	} catch (IOException e) {
	    System.out.println("Connection error..." + e.getMessage());
	}

    }

    public void run() {
	while (thread != null) {
	    try {
		addThread(server.accept());
	    } catch (IOException e) {
		System.out.println("runtime error");
		e.printStackTrace();
	    }
	}

    }

    // Begin session with client in a separate thread
    private void addThread(Socket socket) {
	client = new Session(socket);
	try {
	    client.open();
	    client.start();
	} catch (IOException e) {
	    System.out.println("Could not start session with client");
	    e.printStackTrace();
	}
    }

    public void start() {
	if (thread == null) {
	    thread = new Thread(this);
	    thread.start();
	}
    }

    public void stop() {
	if (thread != null) {
	    thread = null;
	}
    }

    public static void main(String[] args) {
	new ChatServer(999);
    }

}