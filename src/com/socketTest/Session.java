package com.socketTest;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class Session extends Thread {
    private Socket socket;
    private String name;
    private DataInputStream in;

    public Session(Socket socket) {
	this.socket = socket;
    }

    public void run() {
	try {
	    name = in.readUTF();
	    System.out.println(name + " has joined.");
	    
	    while (true) {
		System.out.println(name + ": " + in.readUTF());
	    }
	} catch (IOException e) {
	    System.out.println(name + " has left.");
	}
    }

    public void open() throws IOException {
	in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
    }

    public void close() throws IOException {
	if (socket != null)
	    socket.close();
	if (in != null)
	    in.close();
    }
}
