package com.socketTest;

import java.awt.HeadlessException;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

public class ChatClient {
    private Socket socket = null;
    private DataOutputStream out = null;

    public ChatClient(String clientName) {
	boolean error = false;

	try {
	    socket = new Socket("192.168.0.2", 999);
	    start();
	    // Inform server of clients name
	    out.writeUTF(clientName);
	    out.flush();
	} catch (UnknownHostException uhe) {
	    System.out.println(uhe.getMessage());
	    error = true;
	} catch (IOException ioe) {
	    System.out.println("\nServer Down?");
	    error = true;
	}

	if (!error) {
	    String message = "";
	    while (!message.equalsIgnoreCase("exit")) {
		try {
		    System.out.print("\n:");
		    message = System.console().readLine();
		    out.writeUTF(message);
		    out.flush();
		} catch (IOException e) {
		    System.out.println("Error sending message..." + e.getMessage());
		}
	    }
	}
    }// chatClient

    public void start() throws IOException {
	out = new DataOutputStream(socket.getOutputStream());
    }

    public static void main(String[] args) throws HeadlessException, UnknownHostException, IOException {
	System.out.println("\tWelcome to the chat room!\n\tType exit when you are finished chatting :)\n\n");
	new JOptionPane();
	new ChatClient(JOptionPane.showInputDialog("What's your name?"));
    }
}
